.PHONY: run
run:
	PORT=8080 node app.js

.PHONY: deploy
deploy:
	git push heroku master

.PHONY: curl-test
curl-test:
	curl --header "Content-Type: application/json" \
	  --request POST \
	  --data "$$(cat example-request.json)" \
	  http://localhost:8080/alice/

.PHONY: echo
echo:
	echo $$(cat example-request.json)
