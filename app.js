const express = require('express');
const bodyParser = require('body-parser');
const makeResponse = require('./make-response.js');

const app = express();

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send(Date.now().toString());
});

app.post('/alice', (req, res) => {
  const { session } = req.body;
  res.send(makeResponse(session.session_id, session.message_id, session.user_id));
});

const PORT = process.env.PORT || 80;

app.listen(PORT, () => {
  console.log(`App listening on ${PORT} port`);
});
