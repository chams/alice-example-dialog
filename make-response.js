const getTemplate = () => {
  return {
    "response": {
      "text": "Привет мир!",
      "end_session": true
    },
      "session": {
        "session_id": null,
        "message_id": null,
        "user_id": null
      },
      "version": "1.0"
  };
};

module.exports = (sessionId, messageId, userId) => {
  return Object.assign(getTemplate(), {
    session: {
      session_id: sessionId,
      message_id: messageId,
      user_id: userId 
    }
  }); 
}
